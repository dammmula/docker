FROM nginx

COPY assets /usr/share/nginx/html/assets
COPY styles /usr/share/nginx/html/styles
COPY index.html /usr/share/nginx/html

EXPOSE 80